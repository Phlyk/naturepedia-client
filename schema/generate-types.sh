#!/bin/bash

SCRIPT_DIR=$( cd $(dirname $0); pwd -P )

BACKEND_SCHEMA="${SCRIPT_DIR}/generated/graphql-schema.json"
INTROSPECTED_SCHEMA="${SCRIPT_DIR}/generated/introspected-schema.json"
DEFAULT_OUTPUT_DIR=${SCRIPT_DIR}/../src/app/gql-types/graphql-types.ts
LB_STYLE_OUTPUT_DIR=${SCRIPT_DIR}/../src/app/gql-types/graphql-types.ts


### Suggested Gen command
npx apollo codegen:generate \
--includes="${SCRIPT_DIR}/../src/**/*.graphql.ts" \
--localSchemaFile=${INTROSPECTED_SCHEMA} \
--target=typescript
# --addTypename - defults to true



### LB Gen command
# npx apollo codegen:generate ${LB_STYLE_OUTPUT_DIR} \
# --queries=**/*.graphql.ts \
# --localSchemaFile=${BACKEND_SCHEMA} \
# --target=typescript \
# --outputFlat \


