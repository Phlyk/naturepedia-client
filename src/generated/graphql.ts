import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
import { FishDataModule } from 'src/app/fish/fish-data.module';

export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /**
   * The `DateTime` scalar type represents a DateTime
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  DateTime: any;
};


export type AddFish = {
  __typename?: 'AddFish';
  fish?: Maybe<FishType>;
};

export type BulkUpdateFish = {
  __typename?: 'BulkUpdateFish';
  nbFishUpdated?: Maybe<Scalars['Int']>;
};

/** An enumeration. */
export enum ColourEntityColour {
  /** No-colour */
  None = 'NONE',
  /** Red */
  Red = 'RED',
  /** Blue */
  Blue = 'BLUE',
  /** Yellow */
  Yellow = 'YELLOW',
  /** Purpz */
  Purp = 'PURP',
  /** Green */
  Green = 'GREEN',
  /** Black */
  Black = 'BLACK',
  /** White */
  White = 'WHITE',
  /** Grey */
  Grey = 'GREY'
}

export type ColourInput = {
  colour?: Maybe<ColourInputEnum>;
};

export enum ColourInputEnum {
  None = 'NONE',
  Red = 'RED',
  Blue = 'BLUE',
  Yellow = 'YELLOW',
  Purp = 'PURP',
  Green = 'GREEN',
  Black = 'BLACK',
  White = 'WHITE',
  Grey = 'GREY'
}

export type ColourType = Node & {
  __typename?: 'ColourType';
  createdOn: Scalars['DateTime'];
  updatedOn: Scalars['DateTime'];
  deletedOn?: Maybe<Scalars['DateTime']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  colour: ColourEntityColour;
  fishSet: FishTypeConnection;
};


export type ColourTypeFishSetArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  englishName?: Maybe<Scalars['String']>;
  englishName_Icontains?: Maybe<Scalars['String']>;
  englishName_Istartswith?: Maybe<Scalars['String']>;
  colour_Colour_Istartswith?: Maybe<Scalars['String']>;
  size_MinHeight_Gt?: Maybe<Scalars['Int']>;
};


export type DeleteFishById = {
  __typename?: 'DeleteFishById';
  nbDeletedFish?: Maybe<Scalars['Int']>;
};

export type DeleteFishExclusive = {
  __typename?: 'DeleteFishExclusive';
  nbDeletedFish?: Maybe<Scalars['Int']>;
};

export type DeleteFishInclusive = {
  __typename?: 'DeleteFishInclusive';
  nbDeletedFish?: Maybe<Scalars['Int']>;
};

export type DeleteFishInput = {
  size?: Maybe<SizeInput>;
  englishName?: Maybe<Scalars['String']>;
  /** what do them sciency folks call it? */
  scientificName?: Maybe<Scalars['String']>;
  consumes?: Maybe<Scalars['String']>;
  colour?: Maybe<ColourInput>;
  evolvedIn?: Maybe<Scalars['Int']>;
  /** how many fins yer wee fishy have? */
  nbFins?: Maybe<Scalars['Int']>;
};

export type FishInput = {
  size?: Maybe<SizeInput>;
  englishName: Scalars['String'];
  /** what do them sciency folks call it? */
  scientificName?: Maybe<Scalars['String']>;
  consumes: Scalars['String'];
  colour?: Maybe<ColourInput>;
  evolvedIn: Scalars['Int'];
  /** how many fins yer wee fishy have? */
  nbFins?: Maybe<Scalars['Int']>;
};

export type FishType = Node & {
  __typename?: 'FishType';
  createdOn: Scalars['DateTime'];
  updatedOn: Scalars['DateTime'];
  deletedOn?: Maybe<Scalars['DateTime']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  englishName: Scalars['String'];
  scientificName: Scalars['String'];
  size: SizeType;
  consumes: Scalars['String'];
  evolvedIn?: Maybe<Scalars['String']>;
  colour: ColourType;
  nbFins: Scalars['Int'];
};

export type FishTypeConnection = {
  __typename?: 'FishTypeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<FishTypeEdge>>;
};

/** A Relay edge containing a `FishType` and its cursor. */
export type FishTypeEdge = {
  __typename?: 'FishTypeEdge';
  /** The item at the end of the edge */
  node?: Maybe<FishType>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type MakeSquibildyPuff = {
  __typename?: 'MakeSquibildyPuff';
  squibildyPuff?: Maybe<SquibildyPuff>;
};

export type MutationsOfHope = {
  __typename?: 'MutationsOfHope';
  makeMeASquibildyPuff?: Maybe<MakeSquibildyPuff>;
  /** add a fish using relay */
  relayAddFish?: Maybe<RelayAddFishPayload>;
  /** the endpoint to add a fish */
  addFish?: Maybe<AddFish>;
  /** That fish need updatin' */
  updateFishById?: Maybe<UpdateFishById>;
  /** When Ye need to update more than one fishy type */
  bulkUpdateFish?: Maybe<BulkUpdateFish>;
  /** Delete fish where the fish must match a combination of a fields */
  deleteFishInclusiveSearch?: Maybe<DeleteFishInclusive>;
  /** Delete fish where every fish matching a field is deleted */
  deleteFishExclusiveSearch?: Maybe<DeleteFishExclusive>;
  /** Pass the ID of the fish to go boom & it will */
  deleteFishById?: Maybe<DeleteFishById>;
};


export type MutationsOfHopeMakeMeASquibildyPuffArgs = {
  name: Scalars['String'];
  randomtextbase?: Maybe<Scalars['String']>;
};


export type MutationsOfHopeRelayAddFishArgs = {
  input: RelayAddFishInput;
};


export type MutationsOfHopeAddFishArgs = {
  fishInput: FishInput;
};


export type MutationsOfHopeUpdateFishByIdArgs = {
  fishId: Scalars['ID'];
  fishInput: FishInput;
};


export type MutationsOfHopeBulkUpdateFishArgs = {
  fishFieldsToUpdate: FishInput;
  fishSearch: FishInput;
};


export type MutationsOfHopeDeleteFishInclusiveSearchArgs = {
  deleteFishInput: DeleteFishInput;
};


export type MutationsOfHopeDeleteFishExclusiveSearchArgs = {
  deleteFishInput: DeleteFishInput;
};


export type MutationsOfHopeDeleteFishByIdArgs = {
  fishId: Scalars['ID'];
};

/** An object with an ID */
export type Node = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

/** The Relay compliant `PageInfo` type, containing data necessary to paginate this connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['String']>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['String']>;
};

export type QueriesOfDoom = {
  __typename?: 'QueriesOfDoom';
  mySquibildyPuff?: Maybe<SquibildyPuff>;
  /** The ID of the object */
  fishNode?: Maybe<FishType>;
  allFishConnection?: Maybe<FishTypeConnection>;
  allFishiesList?: Maybe<Array<Maybe<FishType>>>;
  fishById?: Maybe<FishType>;
  fishByConsumes?: Maybe<FishType>;
};


export type QueriesOfDoomMySquibildyPuffArgs = {
  squibName?: Maybe<Scalars['String']>;
  baseText?: Maybe<Scalars['String']>;
  sizeOfText?: Maybe<Scalars['Int']>;
};


export type QueriesOfDoomFishNodeArgs = {
  id: Scalars['ID'];
};


export type QueriesOfDoomAllFishConnectionArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  englishName?: Maybe<Scalars['String']>;
  englishName_Icontains?: Maybe<Scalars['String']>;
  englishName_Istartswith?: Maybe<Scalars['String']>;
  colour_Colour_Istartswith?: Maybe<Scalars['String']>;
  size_MinHeight_Gt?: Maybe<Scalars['Int']>;
};


export type QueriesOfDoomFishByIdArgs = {
  globalId: Scalars['ID'];
};


export type QueriesOfDoomFishByConsumesArgs = {
  consumes: Scalars['String'];
};

export type RelayAddFishInput = {
  /** the fields required to create a fish */
  fishInput: FishInput;
  /** just testing */
  random?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type RelayAddFishPayload = {
  __typename?: 'RelayAddFishPayload';
  fish?: Maybe<FishType>;
  ok?: Maybe<Scalars['Boolean']>;
  errors: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SizeInput = {
  minHeight: Scalars['Int'];
  maxHeight: Scalars['Int'];
  avgHeight?: Maybe<Scalars['Int']>;
  minWeight?: Maybe<Scalars['Int']>;
  maxWeight?: Maybe<Scalars['Int']>;
  avgWeight?: Maybe<Scalars['Int']>;
};

export type SizeType = Node & {
  __typename?: 'SizeType';
  /** The ID of the object. */
  id: Scalars['ID'];
  createdOn: Scalars['DateTime'];
  updatedOn: Scalars['DateTime'];
  deletedOn?: Maybe<Scalars['DateTime']>;
  /** in mm */
  minHeight: Scalars['Int'];
  /** in mm */
  maxHeight: Scalars['Int'];
  /** in mm */
  avgHeight: Scalars['Int'];
  /** in grams */
  minWeight: Scalars['Int'];
  /** in grams */
  maxWeight: Scalars['Int'];
  /** in grams */
  avgWeight: Scalars['Int'];
  fishSet: FishTypeConnection;
};


export type SizeTypeFishSetArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  englishName?: Maybe<Scalars['String']>;
  englishName_Icontains?: Maybe<Scalars['String']>;
  englishName_Istartswith?: Maybe<Scalars['String']>;
  colour_Colour_Istartswith?: Maybe<Scalars['String']>;
  size_MinHeight_Gt?: Maybe<Scalars['Int']>;
};

export type SquibildyPuff = {
  __typename?: 'SquibildyPuff';
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  randomtext?: Maybe<Scalars['String']>;
  fullName?: Maybe<Scalars['String']>;
};

export type UpdateFishById = {
  __typename?: 'UpdateFishById';
  fishUpdated?: Maybe<FishType>;
};

export type FullFishFragmentFragment = (
  { __typename?: 'FishType' }
  & Pick<FishType, 'id' | 'englishName' | 'scientificName' | 'evolvedIn' | 'createdOn' | 'consumes'>
  & { size: (
    { __typename?: 'SizeType' }
    & Pick<SizeType, 'id' | 'minHeight' | 'maxHeight' | 'avgHeight' | 'minWeight' | 'avgWeight'>
  ), colour: (
    { __typename?: 'ColourType' }
    & Pick<ColourType, 'colour'>
  ) }
);

export type StandardFishFragment = (
  { __typename?: 'FishType' }
  & Pick<FishType, 'id' | 'englishName' | 'scientificName' | 'evolvedIn' | 'consumes'>
  & { size: (
    { __typename?: 'SizeType' }
    & Pick<SizeType, 'minHeight' | 'maxHeight'>
  ), colour: (
    { __typename?: 'ColourType' }
    & Pick<ColourType, 'colour'>
  ) }
);

export type GetMeAllDemFishesQueryVariables = {};


export type GetMeAllDemFishesQuery = (
  { __typename?: 'QueriesOfDoom' }
  & { allFishiesList?: Maybe<Array<Maybe<(
    { __typename?: 'FishType' }
    & StandardFishFragment
  )>>> }
);

export type GetMeAFishByWhatItEatsQueryVariables = {
  consumes: Scalars['String'];
};


export type GetMeAFishByWhatItEatsQuery = (
  { __typename?: 'QueriesOfDoom' }
  & { fishByConsumes?: Maybe<(
    { __typename?: 'FishType' }
    & StandardFishFragment
  )> }
);

export type GetMeAFishByIdQueryVariables = {
  id: Scalars['ID'];
};


export type GetMeAFishByIdQuery = (
  { __typename?: 'QueriesOfDoom' }
  & { fishById?: Maybe<(
    { __typename?: 'FishType' }
    & StandardFishFragment
  )> }
);

export type MakeMeASqwisherFishMutationVariables = {
  fishInput: FishInput;
};


export type MakeMeASqwisherFishMutation = (
  { __typename?: 'MutationsOfHope' }
  & { addFish?: Maybe<(
    { __typename?: 'AddFish' }
    & { fish?: Maybe<(
      { __typename?: 'FishType' }
      & FullFishFragmentFragment
    )> }
  )> }
);

export type UpdateYerrrFishMutationVariables = {
  fishId: Scalars['ID'];
  fishInputData: FishInput;
};


export type UpdateYerrrFishMutation = (
  { __typename?: 'MutationsOfHope' }
  & { updateFishById?: Maybe<(
    { __typename?: 'UpdateFishById' }
    & { fishUpdated?: Maybe<(
      { __typename: 'FishType' }
      & Pick<FishType, 'id' | 'createdOn' | 'englishName'>
      & { colour: (
        { __typename?: 'ColourType' }
        & Pick<ColourType, 'colour'>
      ) }
    )> }
  )> }
);

export type UpdateManyFishAtWunceMutationVariables = {
  fishToRefineSearch: FishInput;
  fishPropertiesToUpdate: FishInput;
};


export type UpdateManyFishAtWunceMutation = (
  { __typename?: 'MutationsOfHope' }
  & { bulkUpdateFish?: Maybe<(
    { __typename?: 'BulkUpdateFish' }
    & Pick<BulkUpdateFish, 'nbFishUpdated'>
  )> }
);

export type DeleteMeFishInclusiveMutationVariables = {
  deleteFishInputIncl: DeleteFishInput;
};


export type DeleteMeFishInclusiveMutation = (
  { __typename?: 'MutationsOfHope' }
  & { deleteFishInclusiveSearch?: Maybe<(
    { __typename?: 'DeleteFishInclusive' }
    & Pick<DeleteFishInclusive, 'nbDeletedFish'>
  )> }
);

export type DeleteMeFishExclusiveMutationVariables = {
  deleteFishInputExcl: DeleteFishInput;
};


export type DeleteMeFishExclusiveMutation = (
  { __typename?: 'MutationsOfHope' }
  & { deleteFishExclusiveSearch?: Maybe<(
    { __typename?: 'DeleteFishExclusive' }
    & Pick<DeleteFishExclusive, 'nbDeletedFish'>
  )> }
);

export type DeleteByIdMutationVariables = {
  fishId: Scalars['ID'];
  fishName?: Maybe<Scalars['String']>;
};


export type DeleteByIdMutation = (
  { __typename?: 'MutationsOfHope' }
  & { deleteFishById?: Maybe<(
    { __typename?: 'DeleteFishById' }
    & Pick<DeleteFishById, 'nbDeletedFish'>
  )> }
);

export type SquigglesFragment = (
  { __typename?: 'SquibildyPuff' }
  & Pick<SquibildyPuff, 'id' | 'name' | 'fullName' | 'randomtext'>
);

export type GimmeAPrimeSquibPuffQueryVariables = {
  squibName?: Maybe<Scalars['String']>;
  baseText: Scalars['String'];
  sizeOfText?: Maybe<Scalars['Int']>;
};


export type GimmeAPrimeSquibPuffQuery = (
  { __typename?: 'QueriesOfDoom' }
  & { mySquibildyPuff?: Maybe<(
    { __typename?: 'SquibildyPuff' }
    & SquigglesFragment
  )> }
);

export type MakeMeASquibilidyPuffMutationVariables = {
  squibName: Scalars['String'];
  randomtextbase?: Maybe<Scalars['String']>;
};


export type MakeMeASquibilidyPuffMutation = (
  { __typename?: 'MutationsOfHope' }
  & { makeMeASquibildyPuff?: Maybe<(
    { __typename?: 'MakeSquibildyPuff' }
    & { squibildyPuff?: Maybe<(
      { __typename?: 'SquibildyPuff' }
      & SquigglesFragment
    )> }
  )> }
);

export namespace FullFishFragment {
  export type Fragment = FullFishFragmentFragment;
  export type Size = FullFishFragmentFragment['size'];
  export type Colour = FullFishFragmentFragment['colour'];
}

export namespace StandardFish {
  export type Fragment = StandardFishFragment;
  export type Size = StandardFishFragment['size'];
  export type Colour = StandardFishFragment['colour'];
}

export namespace GetMeAllDemFishes {
  export type Variables = GetMeAllDemFishesQueryVariables;
  export type Query = GetMeAllDemFishesQuery;
  export type AllFishiesList = GetMeAllDemFishesQuery['allFishiesList'][0];
}

export namespace GetMeAFishByWhatItEats {
  export type Variables = GetMeAFishByWhatItEatsQueryVariables;
  export type Query = GetMeAFishByWhatItEatsQuery;
  export type FishByConsumes = GetMeAFishByWhatItEatsQuery['fishByConsumes'];
}

export namespace GetMeAFishById {
  export type Variables = GetMeAFishByIdQueryVariables;
  export type Query = GetMeAFishByIdQuery;
  export type FishById = GetMeAFishByIdQuery['fishById'];
}

export namespace MakeMeASqwisherFish {
  export type Variables = MakeMeASqwisherFishMutationVariables;
  export type Mutation = MakeMeASqwisherFishMutation;
  export type AddFish = MakeMeASqwisherFishMutation['addFish'];
  export type Fish = MakeMeASqwisherFishMutation['addFish']['fish'];
}

export namespace UpdateYerrrFish {
  export type Variables = UpdateYerrrFishMutationVariables;
  export type Mutation = UpdateYerrrFishMutation;
  export type UpdateFishById = UpdateYerrrFishMutation['updateFishById'];
  export type FishUpdated = UpdateYerrrFishMutation['updateFishById']['fishUpdated'];
  export type Colour = UpdateYerrrFishMutation['updateFishById']['fishUpdated']['colour'];
}

export namespace UpdateManyFishAtWunce {
  export type Variables = UpdateManyFishAtWunceMutationVariables;
  export type Mutation = UpdateManyFishAtWunceMutation;
  export type BulkUpdateFish = UpdateManyFishAtWunceMutation['bulkUpdateFish'];
}

export namespace DeleteMeFishInclusive {
  export type Variables = DeleteMeFishInclusiveMutationVariables;
  export type Mutation = DeleteMeFishInclusiveMutation;
  export type DeleteFishInclusiveSearch = DeleteMeFishInclusiveMutation['deleteFishInclusiveSearch'];
}

export namespace DeleteMeFishExclusive {
  export type Variables = DeleteMeFishExclusiveMutationVariables;
  export type Mutation = DeleteMeFishExclusiveMutation;
  export type DeleteFishExclusiveSearch = DeleteMeFishExclusiveMutation['deleteFishExclusiveSearch'];
}

export namespace DeleteById {
  export type Variables = DeleteByIdMutationVariables;
  export type Mutation = DeleteByIdMutation;
  export type DeleteFishById = DeleteByIdMutation['deleteFishById'];
}

export namespace Squiggles {
  export type Fragment = SquigglesFragment;
}

export namespace GimmeAPrimeSquibPuff {
  export type Variables = GimmeAPrimeSquibPuffQueryVariables;
  export type Query = GimmeAPrimeSquibPuffQuery;
  export type MySquibildyPuff = GimmeAPrimeSquibPuffQuery['mySquibildyPuff'];
}

export namespace MakeMeASquibilidyPuff {
  export type Variables = MakeMeASquibilidyPuffMutationVariables;
  export type Mutation = MakeMeASquibilidyPuffMutation;
  export type MakeMeASquibildyPuff = MakeMeASquibilidyPuffMutation['makeMeASquibildyPuff'];
  export type SquibildyPuff = MakeMeASquibilidyPuffMutation['makeMeASquibildyPuff']['squibildyPuff'];
}

export const FullFishFragmentFragmentDoc = gql`
    fragment fullFishFragment on FishType {
  id
  englishName
  scientificName
  evolvedIn
  size {
    id
    minHeight
    maxHeight
    avgHeight
    minWeight
    maxHeight
    avgWeight
  }
  colour {
    colour
  }
  createdOn
  consumes
}
    `;
export const StandardFishFragmentDoc = gql`
    fragment standardFish on FishType {
  id
  englishName
  scientificName
  evolvedIn
  size {
    minHeight
    maxHeight
  }
  colour {
    colour
  }
  consumes
}
    `;
export const SquigglesFragmentDoc = gql`
    fragment squiggles on SquibildyPuff {
  id
  name
  fullName
  randomtext
}
    `;
export const GetMeAllDemFishesDocument = gql`
    query GetMeAllDemFishes {
  allFishiesList {
    ...standardFish
  }
}
    ${StandardFishFragmentDoc}`;

@Injectable({
    providedIn: FishDataModule,
})
export class GetMeAllDemFishesGQL extends Apollo.Query<GetMeAllDemFishesQuery, GetMeAllDemFishesQueryVariables> {
  document = GetMeAllDemFishesDocument;
}
export const GetMeAFishByWhatItEatsDocument = gql`
    query getMeAFishByWhatItEats($consumes: String!) {
  fishByConsumes(consumes: "Everything in the water") {
    ...standardFish
  }
}
    ${StandardFishFragmentDoc}`;

@Injectable({
    providedIn: 'root',
})
export class GetMeAFishByWhatItEatsGQL extends Apollo.Query<GetMeAFishByWhatItEatsQuery, GetMeAFishByWhatItEatsQueryVariables> {
  document = GetMeAFishByWhatItEatsDocument;
}
export const GetMeAFishByIdDocument = gql`
    query getMeAFishById($id: ID!) {
  fishById(globalId: $id) {
    ...standardFish
  }
}
    ${StandardFishFragmentDoc}`;

@Injectable({
    providedIn: 'root',
})
export class GetMeAFishByIdGQL extends Apollo.Query<GetMeAFishByIdQuery, GetMeAFishByIdQueryVariables> {
  document = GetMeAFishByIdDocument;
}
export const MakeMeASqwisherFishDocument = gql`
    mutation makeMeASqwisherFish($fishInput: FishInput!) {
  addFish(fishInput: $fishInput) {
    fish {
      ...fullFishFragment
    }
  }
}
    ${FullFishFragmentFragmentDoc}`;

@Injectable({
    providedIn: 'root',
})
export class MakeMeASqwisherFishGQL extends Apollo.Mutation<MakeMeASqwisherFishMutation, MakeMeASqwisherFishMutationVariables> {
  document = MakeMeASqwisherFishDocument;
}
export const UpdateYerrrFishDocument = gql`
    mutation updateYerrrFish($fishId: ID!, $fishInputData: FishInput!) {
  updateFishById(fishId: $fishId, fishInput: $fishInputData) {
    fishUpdated {
      id
      __typename
      createdOn
      englishName
      colour {
        colour
      }
    }
  }
}
    `;

@Injectable({
  providedIn: 'root',
})
export class UpdateYerrrFishGQL extends Apollo.Mutation<UpdateYerrrFishMutation, UpdateYerrrFishMutationVariables> {
  document = UpdateYerrrFishDocument;
}
export const UpdateManyFishAtWunceDocument = gql`
    mutation updateManyFishAtWunce($fishToRefineSearch: FishInput!, $fishPropertiesToUpdate: FishInput!) {
  bulkUpdateFish(fishSearch: $fishToRefineSearch, fishFieldsToUpdate: $fishPropertiesToUpdate) {
    nbFishUpdated
  }
}
    `;

@Injectable({
    providedIn: 'root',
})
export class UpdateManyFishAtWunceGQL extends Apollo.Mutation<UpdateManyFishAtWunceMutation, UpdateManyFishAtWunceMutationVariables> {
  document = UpdateManyFishAtWunceDocument;
}
export const DeleteMeFishInclusiveDocument = gql`
    mutation deleteMeFishInclusive($deleteFishInputIncl: DeleteFishInput!) {
  deleteFishInclusiveSearch(deleteFishInput: $deleteFishInputIncl) {
    nbDeletedFish
  }
}
    `;

@Injectable({
    providedIn: 'root',
})
export class DeleteMeFishInclusiveGQL extends Apollo.Mutation<DeleteMeFishInclusiveMutation, DeleteMeFishInclusiveMutationVariables> {
  document = DeleteMeFishInclusiveDocument;
}
export const DeleteMeFishExclusiveDocument = gql`
    mutation deleteMeFishExclusive($deleteFishInputExcl: DeleteFishInput!) {
  deleteFishExclusiveSearch(deleteFishInput: $deleteFishInputExcl) {
    nbDeletedFish
  }
}
    `;

@Injectable({
    providedIn: 'root',
})
export class DeleteMeFishExclusiveGQL extends Apollo.Mutation<DeleteMeFishExclusiveMutation, DeleteMeFishExclusiveMutationVariables> {
  document = DeleteMeFishExclusiveDocument;
}
export const DeleteByIdDocument = gql`
    mutation deleteById($fishId: ID!, $fishName: String) {
  deleteFishById(fishId: $fishId) {
    nbDeletedFish
  }
}
    `;

@Injectable({
    providedIn: 'root',
})
export class DeleteByIdGQL extends Apollo.Mutation<DeleteByIdMutation, DeleteByIdMutationVariables> {
  document = DeleteByIdDocument;
}
export const GimmeAPrimeSquibPuffDocument = gql`
    query gimmeAPrimeSquibPuff($squibName: String, $baseText: String!, $sizeOfText: Int) {
  mySquibildyPuff(squibName: $squibName, baseText: $baseText, sizeOfText: $sizeOfText) {
    ...squiggles
  }
}
    ${SquigglesFragmentDoc}`;

@Injectable({
    providedIn: 'root',
})
export class GimmeAPrimeSquibPuffGQL extends Apollo.Query<GimmeAPrimeSquibPuffQuery, GimmeAPrimeSquibPuffQueryVariables> {
  document = GimmeAPrimeSquibPuffDocument;
}
export const MakeMeASquibilidyPuffDocument = gql`
    mutation makeMeASquibilidyPuff($squibName: String!, $randomtextbase: String) {
  makeMeASquibildyPuff(name: $squibName, randomtextbase: $randomtextbase) {
    squibildyPuff {
      ...squiggles
    }
  }
}
    ${SquigglesFragmentDoc}`;

@Injectable({
    providedIn: 'root',
})
export class MakeMeASquibilidyPuffGQL extends Apollo.Mutation<MakeMeASquibilidyPuffMutation, MakeMeASquibilidyPuffMutationVariables> {
  document = MakeMeASquibilidyPuffDocument;
}
