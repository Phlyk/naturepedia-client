import { Injectable } from '@angular/core';
import { FetchResult } from 'apollo-link';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ColourEntityColour, FishInput, GetMeAFishByWhatItEats, GetMeAFishByWhatItEatsGQL, GetMeAllDemFishes, GetMeAllDemFishesDocument, GetMeAllDemFishesGQL, GetMeAllDemFishesQuery, MakeMeASqwisherFish, MakeMeASqwisherFishGQL, MakeMeASqwisherFishMutation, UpdateYerrrFishGQL } from 'src/generated/graphql';

@Injectable(
    // { providedIn: 'root' }
)
export class FishDataService {
    UPDATE_APOLLO_FISH_CACHE_USING_REFETCH = false;

    constructor(
        private getMeAllDemFishesQuery: GetMeAllDemFishesGQL,
        private getMeAParticularFishQuery: GetMeAFishByWhatItEatsGQL,
        private makeMeAFishMutation: MakeMeASqwisherFishGQL,
        private updateYerFishMutation: UpdateYerrrFishGQL,
    ) {
        console.log("THE FISH SERVICE HAS BEEN CONSTRUCTED (just testing lazyloading")
    }

    getMeAllDemFishes(): Observable<GetMeAllDemFishes.AllFishiesList[]> {
        return this.getMeAllDemFishesQuery.watch()
            .valueChanges
            .pipe(
                catchError(err => {
                    console.error(err);
                    return of({
                        data: {
                            allFishiesList: []
                        }
                    });
                }),
                map(payload => payload.data.allFishiesList)
            );
    }

    getMeAParticularFishSon(fishConsumesWhat: GetMeAFishByWhatItEats.Variables): Observable<GetMeAFishByWhatItEats.FishByConsumes> {
        return this.getMeAParticularFishQuery.fetch(fishConsumesWhat)
            .pipe(map(payload => payload.data.fishByConsumes));
    }

    makeMeAFish(fishProperties: FishInput): Observable<MakeMeASqwisherFish.AddFish> {
        let makeMeAFishObservable: Observable<FetchResult<MakeMeASqwisherFish.Mutation>>;
        if (this.UPDATE_APOLLO_FISH_CACHE_USING_REFETCH) {
            makeMeAFishObservable = this.makeMeAFishUpdateCacheWithRefetch(fishProperties);
        } else {
            makeMeAFishObservable = this.makeMeAFishUpdateCacheWithUpdate(fishProperties);
        }
        return makeMeAFishObservable
            .pipe(
                map(payload => payload.data.addFish),
                tap(addFishResponse => console.log(addFishResponse)),
            );
    }

    makeMeAFishUpdateCacheWithUpdate(fishProperties: FishInput): Observable<FetchResult<MakeMeASqwisherFish.Mutation>> {
        console.log("Doing it the update way")
        return this.makeMeAFishMutation.mutate(
            { fishInput: fishProperties },
            {
                optimisticResponse: this.makeOptimisticResponsePayloadForMakeFish(fishProperties),
                update: (proxy, mutationPayload) => {
                    const existingCache = proxy.readQuery<GetMeAllDemFishesQuery, any>({ query: GetMeAllDemFishesDocument });
                    existingCache.allFishiesList.push(mutationPayload.data.addFish.fish);
                    proxy.writeQuery<any, any>({ query: GetMeAllDemFishesDocument, data: existingCache });
                }
            });
    }

    makeMeAFishUpdateCacheWithRefetch(fishProperties: FishInput) {
        console.log("Doing it the refetch way")
        return this.makeMeAFishMutation.mutate(
            { fishInput: fishProperties },
            {
                optimisticResponse: this.makeOptimisticResponsePayloadForMakeFish(fishProperties),
                refetchQueries: [{ query: this.getMeAllDemFishesQuery.document }]
            });
        }
        
    updateMeFish(fishId: string, fishPropsToChange: FishInput) {
        return this.updateYerFishMutation.mutate({
            fishId: fishId,
            fishInputData: fishPropsToChange
        }, {
            optimisticResponse: {
                __typename: "MutationsOfHope",
                updateFishById: {
                    __typename: "UpdateFishById",
                    fishUpdated: {
                        __typename: "FishType",
                        id: fishId,
                        createdOn: "dateofdoom",
                        englishName: fishPropsToChange.englishName,
                        colour: {
                            colour: ColourEntityColour[fishPropsToChange.colour.colour]
                        }
                    }
                }
            }
        }).pipe(
            map(payload => payload.data.updateFishById.fishUpdated),
            tap(updatedFish => console.log("updated fish", updatedFish))
        );
    }

    private makeOptimisticResponsePayloadForMakeFish(fishProperties: FishInput): MakeMeASqwisherFishMutation {
        return {
            __typename: "MutationsOfHope",
            addFish: {
                __typename: "AddFish",
                fish: {
                    __typename: "FishType",
                    id: "100",
                    englishName: fishProperties.englishName,
                    scientificName: "I'm optimistic, I don't know science",
                    consumes: fishProperties.consumes,
                    size: {
                        __typename: "SizeType",
                        id: "100",
                        minHeight: fishProperties.size.minHeight,
                        maxHeight: fishProperties.size.maxHeight,
                        avgHeight: 199,
                        minWeight: 199,
                        avgWeight: 199
                    },
                    createdOn: "nowtime",
                    evolvedIn: String(fishProperties.evolvedIn),
                    colour: {
                        colour: ColourEntityColour[fishProperties.colour.colour]
                    }
                }
            }
        }
    }
}
