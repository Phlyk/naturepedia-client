import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FishDataService } from './fish.data.service';
import { Fish } from './fish.model';
import { FishService } from './fish.service';

@Component({
    selector: 'app-fish',
    templateUrl: './fish.component.html',
    styleUrls: ['./fish.component.scss'],
    providers: [FishService, FishDataService]
})
export class FishComponent implements OnInit {
    fishes: Observable<Fish[]>;
    queryButtonClicked: boolean;

    constructor(private fishService: FishService) { }

    ngOnInit(): void {
        this.fishes = this.fishService.fetchAllFish()
    }

    private async anotherNonSubscribyAsynchronousWayOfFetchingData(): Promise<Fish[]> {
        return await this.fishService.fetchAllFish().toPromise();
    }

    private gettingDataAsyncronouslyButFunctionally(): Fish[] {
        const floop = this.anotherNonSubscribyAsynchronousWayOfFetchingData();
        return null; //this way doesn't help, still need the `then` block
    }
}
