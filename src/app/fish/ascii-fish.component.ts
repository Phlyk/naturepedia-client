

import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'ascii-fish',
    template: `
    <pre>
    o
    o      ______/~/~/~/__           /((
      o  // __            ====__    /_((
     o  //  @))       ))))      ===/__((
        ))           )))))))        __((
        \\     \)     ))))    __===\ _((
         \\_______________====      \_((
                                     \((
    </pre>
    `,
    styles: [``]
})
export class AsciiFishComponent implements OnInit {
    constructor() { }

    ngOnInit(): void { }
}
