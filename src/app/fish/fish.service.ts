import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ColourInput, ColourInputEnum, FishInput } from 'src/generated/graphql';
import { FishDataService } from './fish.data.service';
import { Fish } from './fish.model';

@Injectable()
export class FishService {
    fishes: Fish[];

    private readonly DEFAULT_NB_FINS = 4;

    constructor(private dataService: FishDataService) {

    }

    fetchAllFish(): Observable<Fish[]> {
        return this.dataService.getMeAllDemFishes()
            .pipe(
                tap(fishDtoArray => console.log(fishDtoArray)),
                map(fishDtoArray => {
                    return fishDtoArray.map(fishDto => new Fish(fishDto));
                }),
                tap(fishModels => this.fishes = fishModels)
            );
    }

    getFish(fishIndex: number): Fish {
        return this.fishes[fishIndex];
    }

    makeFish(
        fishName: string,
        fishSizeMax: number,
        fishSizeMin: number,
        fishConsumes: string,
        fishEvolvedIn: number,
        fishColour: string
    ): Observable<Fish> {
        const fishProperties: FishInput = {
            englishName: fishName,
            consumes: fishConsumes,
            evolvedIn: Number(fishEvolvedIn),
            colour: {
                colour: ColourInputEnum[fishColour],
            },
            nbFins: this.DEFAULT_NB_FINS,
            size: {
                maxHeight: Number(fishSizeMax),
                minHeight: Number(fishSizeMin)
            }
        }
        return this.dataService.makeMeAFish(fishProperties)
            .pipe(
                map(addedFish => new Fish(addedFish.fish)),
                tap(addedFish => this.fishes.push(addedFish))
            );
    }

    updateMeFish(fish: Fish): Observable<Fish> {
        const newFishColour: ColourInput = {
            colour: this.changeColourStringToEnum(fish.getColour())
        }
        const randomAssDataToChange: FishInput = {
            englishName: fish.getEnglishName(),
            consumes: "SEA_BOEUF",
            evolvedIn: 123,
            colour: newFishColour
        };
        return this.dataService.updateMeFish(fish.getId(), randomAssDataToChange).pipe(map(fishUpdated => new Fish(fishUpdated)))
    }

    /* 
    HORFFIC function that maps the keys to the values in an array of arrays
    then removes the array where the value corresponds to the `existingColour` param
    then chooses a random enum from the resulting filtered array
    */
    changeColourStringToEnum(existingColour: any): ColourInputEnum {
        const allColourInputKeys: string[] = Object.keys(ColourInputEnum);
        const allColourInputValues = Object.keys(ColourInputEnum)
            .map(key => ColourInputEnum[key])
            .filter(value => typeof value === 'string') as string[];
        const secondColourIndex = allColourInputValues.indexOf(existingColour);

        // const result = Object.assign(...
        const newDictArray = allColourInputKeys.map((key, index) => ([key, allColourInputValues[index]]))
        newDictArray.splice(secondColourIndex, 1);

        const randomKey = Math.floor(Math.random() * newDictArray.length);
        const colourInputEnumKey = newDictArray[randomKey][0];
        const randomColourEnum: ColourInputEnum = ColourInputEnum[colourInputEnumKey];
        return randomColourEnum;
    }
}
