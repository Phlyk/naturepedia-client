import { Component, Input, OnInit } from '@angular/core';
import { Fish } from '../../fish.model';
import { FishService } from '../../fish.service';

@Component({
    selector: 'fish-color-display',
    templateUrl: './fish-color-display.component.html',
    styleUrls: ['./fish-color-display.component.scss']
})
export class FishColorDisplayComponent implements OnInit {

    @Input() fishIndex: number;
    fish: Fish;

    display = {
        fish: false,
        whale: false,
        jellyfish: false,
        shark: false,
        default: false
    };

    constructor(private fishService: FishService) { }

    ngOnInit(): void {
        this.fish = this.fishService.getFish(this.fishIndex);
        this.calculateDisplayType();
    }

    private calculateDisplayType() {
        const le_fish_le_name = this.fish.getEnglishName().toLocaleLowerCase();
        if (le_fish_le_name.includes("fish")) {
            this.display.fish = true;
        } else if (le_fish_le_name.includes("whale")) {
            this.display.whale = true;
        } else if ((le_fish_le_name.includes("jelly")) || (le_fish_le_name.includes("octo"))) {
            this.display.jellyfish = true;
        } else if (le_fish_le_name.includes("shark")) {
            this.display.shark = true;
        } else {
            this.display.default = true;
        }
    }

}
