import { Component, Input, OnInit } from '@angular/core';
import { Fish } from '../fish.model';
import { FishService } from '../fish.service';

@Component({
    selector: 'fish-display',
    templateUrl: './fish-display.component.html',
    styleUrls: ['./fish-display.component.scss']
})
export class FishDisplayComponent implements OnInit {

    @Input() fishIndex: number;
    fish: Fish;

    constructor(private fishService: FishService) { }

    ngOnInit(): void {
        this.fish = this.fishService.getFish(this.fishIndex)
    }

    theyClickedAFish($event, fish: Fish) {
        this.fishService.updateMeFish(fish).subscribe();
    }
}
