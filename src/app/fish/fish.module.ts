import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AsciiFishComponent } from './ascii-fish.component';
import { FishDataModule } from './fish-data.module';
import { FishColorDisplayComponent } from './fish-display/fish-color-display/fish-color-display.component';
import { FishDisplayComponent } from './fish-display/fish-display.component';
import { FishComponent } from './fish.component';
import { MakeFishComponent } from './make-fish/make-fish.component';



@NgModule({
  declarations: [
      FishComponent,
      MakeFishComponent,
      FishDisplayComponent,
      AsciiFishComponent,
      FishColorDisplayComponent
    ],
  imports: [
    CommonModule,
    FishDataModule
  ]
})
export class FishModule { }
