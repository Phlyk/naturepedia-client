import gql from 'graphql-tag';

export const fullFishFragment = gql`
    fragment fullFishFragment on FishType {
        id
        englishName
        scientificName
        evolvedIn
        size {
            id
            minHeight
            maxHeight
            avgHeight
            minWeight
            maxHeight
            avgWeight
        }
        colour {
            colour
        }
        createdOn
        consumes
    }
`;

export const standardFishFragment = gql`
    fragment standardFish on FishType {
        id
        englishName
        scientificName
        evolvedIn
        size {
            minHeight
            maxHeight
        }
        colour {
            colour
        }
        consumes
    }
`

export const GET_ME_ALL_DEM_FISHES = gql`
    query GetMeAllDemFishes {
        allFishiesList @NgModule(module: "src/app/fish/fish-data.module#FishDataModule") {
            ...standardFish
        }
    }
    ${standardFishFragment}
`

export const GET_ME_A_PARTICULAR_FISH_BY_WHAT_IT_EATS = gql`
    query getMeAFishByWhatItEats($consumes: String!) {
        fishByConsumes(consumes: "Everything in the water") {
            ...standardFish
        }
    }
    ${standardFishFragment}
`

export const GET_ME_A_PARTICULAR_FISH_BY_ID = gql`
    query getMeAFishById($id: ID!) {
        fishById(globalId: $id) {
            ...standardFish
        }
    }
    ${standardFishFragment}
`


export const MAKE_ME_A_FISH = gql`
    mutation makeMeASqwisherFish($fishInput: FishInput!) {
        addFish(fishInput: $fishInput) {
            fish {
                ...fullFishFragment
            }
        }
    }
    ${fullFishFragment}
`

export const UPDATE_ME_A_FISH = gql`
    mutation updateYerrrFish($fishId: ID!, $fishInputData: FishInput!) {
        updateFishById(
            fishId: $fishId,
            fishInput: $fishInputData
        ) {
            fishUpdated {
                id
                __typename
                createdOn
                englishName
                colour {
                    colour
                }
            }
        }
    }
`

export const BULK_UPDATE_YE_FISH = gql`
    mutation updateManyFishAtWunce(
        $fishToRefineSearch: FishInput!,
        $fishPropertiesToUpdate: FishInput!
    ) {
        bulkUpdateFish(
            fishSearch: $fishToRefineSearch, 
            fishFieldsToUpdate: $fishPropertiesToUpdate
        ) {
            nbFishUpdated
        }
    }
`

export const DELETE_ME_ALL_THE_FISH_THAT_MATCH_ALL = gql`
    mutation deleteMeFishInclusive($deleteFishInputIncl: DeleteFishInput!) {
        deleteFishInclusiveSearch(deleteFishInput: $deleteFishInputIncl) {
            nbDeletedFish
        }
    }
`

export const DELETE_ME_ALL_THE_FISH_THAT_MATCH_ANY = gql`
    mutation deleteMeFishExclusive($deleteFishInputExcl: DeleteFishInput!) {
        deleteFishExclusiveSearch(deleteFishInput: $deleteFishInputExcl) {
            nbDeletedFish
        }
    }
`

export const DELETE_ME_THAT_ONE_FISH = gql`
    mutation deleteById($fishId: ID!, $fishName: String) {
        deleteFishById(fishId: $fishId) {
            nbDeletedFish
        }
    }
`
