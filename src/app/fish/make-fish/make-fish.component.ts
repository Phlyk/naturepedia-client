import { Component, OnInit } from '@angular/core';
import { ColourInputEnum } from 'src/generated/graphql';
import { FishService } from '../fish.service';

@Component({
    selector: 'make-fish',
    templateUrl: './make-fish.component.html',
    styleUrls: ['./make-fish.component.scss']
})
export class MakeFishComponent implements OnInit {

    fishColours = Object.keys(ColourInputEnum);
    makeFishSuccess: boolean = false;

    constructor(private fishService: FishService) { }

    ngOnInit(): void {
    }

    addFish(fishName, fishSizeMax, fishSizeMin, fishConsumes, fishEvolvedIn, fishColourSelect) {
        this.makeFishSuccess = false;
        this.fishService.makeFish(
            fishName.value,
            fishSizeMax.value,
            fishSizeMin.value,
            fishConsumes.value,
            fishEvolvedIn.value,
            fishColourSelect.value
        )
            .subscribe(addFishPayload => {
                console.log("I got the fish!")
                console.log(addFishPayload)
                this.makeFishSuccess = true;
            },
                error => {
                    console.error(error);
                },
                () => {
                    console.log("I've finished making a fish!")
                }
            );
            // .add(
            //     console.log("I'm finished foo")
            // );
    }

}
