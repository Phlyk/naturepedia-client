import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeFishComponent } from './make-fish.component';

describe('MakeFishComponent', () => {
  let component: MakeFishComponent;
  let fixture: ComponentFixture<MakeFishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeFishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeFishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
