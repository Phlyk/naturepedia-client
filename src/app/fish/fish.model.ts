import { FishType } from 'src/generated/graphql';

export interface ISize {
    __typename?: string;
    id?: string;
    createdOn?: any;
    updatedOn?: any;
    deletedOn?: any;
    minHeight?: number;
    maxHeight?: number;
    avgHeight?: number;
    minWeight?: number;
    maxWeight?: number;
    avgWeight?: number;
}

export interface IFish {
    __typename?: string;
    id?: string;
    englishName?: string;
    scientificName?: string;
    createdOn?: any;
    updatedOn?: any;
    deletedOn?: any;
    size?: ISize;
    consumes?: string;
    evolvedIn?: number;
    colour?: string;
    nbFins?: number;
}

export class Fish {

    fishState: Partial<FishType>;

    constructor(fishyBlobOData: Partial<FishType>) {
        this.fishState = fishyBlobOData;
    }

    getId(): string {
        return this.fishState.id;
    }

    getEnglishName(): string {
        return this.fishState.englishName;
    }

    getColour(): string {
        return this.fishState.colour.colour;
    }

    getScientificName(): string {
        return this.fishState.scientificName;
    }
}

export class FishTwo {
    constructor(
        private id: string,
        private englishName: string,
        private colour: string,
        private scientificName: string,
        private size: ISize,
        private nbFins: number
    ) {

    }
}

export class FishThree {
    constructor(
        private fishDto: Partial<FishType>
    ) {}
}

export abstract class Mapper<D, M> {
    domainModel: M;
    dto: D;

    abstract convertToDto(): D;

    abstract convertToModel(): M;
}

export class FishMapper extends Mapper<FishType, Fish> {
    convertToDto(): FishType {
        throw new Error("Method not implemented.");
    }
    convertToModel(): Fish {
        throw new Error("Method not implemented.");
    }
}
