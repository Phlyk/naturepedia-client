import { SquibildyPuff } from 'src/generated/graphql';

export class SquibildyPuffModel {

    constructor(private squibilidyPuff: SquibildyPuff) {

    }

    chunkSubstr(size: number): string[] {
        const numChunks = Math.ceil(this.squibilidyPuff.fullName.length / size)
        const chunks = new Array(numChunks)

        for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
            chunks[i] = this.squibilidyPuff.fullName.substr(o, size)
        }

        return chunks
    }
}
