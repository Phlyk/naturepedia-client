import gql from 'graphql-tag'

export const squibildyPuffFragment = gql`
    fragment squiggles on SquibildyPuff {
        id
        name
        fullName
        randomtext
    }
`

export const GIMME_A_PRIME_SQUIB_MC_PUFF = gql`
    query gimmeAPrimeSquibPuff($squibName: String, $baseText: String!, $sizeOfText: Int) {
        mySquibildyPuff(squibName: $squibName, baseText: $baseText, sizeOfText: $sizeOfText) {
            ...squiggles
        }
    }
    ${squibildyPuffFragment}
`

export const MAKE_ME_A_SQUIBILIDY_PUFF = gql`
    mutation makeMeASquibilidyPuff($squibName: String!, $randomtextbase: String) {
        makeMeASquibildyPuff(name: $squibName, randomtextbase: $randomtextbase) {
            squibildyPuff {
                ...squiggles
            }
        }
    }
    ${squibildyPuffFragment}
`
