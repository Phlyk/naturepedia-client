import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'squibilidy-puff-input',
    template: `<p>himateidontwork</p>`
    // <form [formGroup]="squibInputFormGroup" class="fill-width">
    //     <input #squibFormNameInput class="squib-form-input" formControlName="query" [placeholder]="placeholder" />
    //     <input #squibFormYarpyInput class="squib-form-input" formControlName="blery" [placeholder]="'hehe ' + placeholder + ' hehe'" />
    // </form>
    // `,
})
export class SquibilidyPuffInputComponent implements OnInit, AfterViewInit {
    // Future optimisation - https://stackoverflow.com/a/39693615/5760182
    @ViewChild('squibFormNameInput') squibFormNameInput: ElementRef;
    @ViewChild('squibFormYarpyInput') squibFormYarpyInput: ElementRef;
    squibInputFormGroup: FormGroup;

    @Input() placeholder: string;

    
    squibSearchFormControlQuery: FormControl;
    squibSearchFormControlBlery: FormControl;
    
    
    constructor() { }
    
    ngOnInit(): void { 
        this.squibSearchFormControlQuery = new FormControl('');
        this.squibSearchFormControlBlery = new FormControl('');
        this.squibInputFormGroup = new FormGroup({
            query: this.squibSearchFormControlQuery,
            blery: this.squibSearchFormControlBlery
        });
    }

    ngAfterViewInit() {
        if (this.squibFormNameInput) {
            setTimeout(() => {
                this.squibFormNameInput.nativeElement.focus();
            }, 400);
        }
    }
}
