import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GimmeAPrimeSquibPuffGQL, GimmeAPrimeSquibPuffQuery, GimmeAPrimeSquibPuffQueryVariables, MakeMeASquibilidyPuffGQL, MakeMeASquibilidyPuffMutationVariables, SquibildyPuff, SquigglesFragment } from 'src/generated/graphql';
import { GIMME_A_PRIME_SQUIB_MC_PUFF } from './squibilidy-puff.service.graphql';

@Injectable({ providedIn: 'root' })
export class SquibilidyPuffDataService {

    constructor(
        private apollo: Apollo,
        private gimmeAPrimeSquibPuffQuery: GimmeAPrimeSquibPuffGQL,
        private makeMeASquibilidyPuffMut: MakeMeASquibilidyPuffGQL
    ) { }

    getMeASquibilidyPuff(fetchSquibPuffVariables: GimmeAPrimeSquibPuffQueryVariables): Observable<SquigglesFragment> {
        return this.gimmeAPrimeSquibPuffQuery.fetch(fetchSquibPuffVariables)
            .pipe(map(payload => payload.data.mySquibildyPuff))
    }

    makeMeASquibilidyPuff(makeMeASquibilidyPuff: MakeMeASquibilidyPuffMutationVariables): Observable<SquibildyPuff> {
        return this.makeMeASquibilidyPuffMut.mutate(makeMeASquibilidyPuff)
            .pipe(map(payload => payload.data.makeMeASquibildyPuff.squibildyPuff))
    }

    getMeAnOldSchoolSquibMcPuffingtons(baseText: string, squibName?: string, sizeOfText?: number): Observable<SquibildyPuff> {
        return this.apollo.query<GimmeAPrimeSquibPuffQuery, GimmeAPrimeSquibPuffQueryVariables>({
            query: GIMME_A_PRIME_SQUIB_MC_PUFF,
            variables: {
                baseText: baseText,
                sizeOfText: sizeOfText,
                squibName: squibName
            }
        })
        .pipe(map(payload => payload.data.mySquibildyPuff));
        // .pipe(map(({data}) => data.mySquibildyPuff));
        // .pipe(pluck('data', 'mySqubilidyPuff'));
        // All equiv

    }
}
