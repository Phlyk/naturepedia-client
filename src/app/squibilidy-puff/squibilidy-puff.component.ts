import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GimmeAPrimeSquibPuffQueryVariables, MakeMeASquibilidyPuffMutationVariables } from 'src/generated/graphql';
import { SquibildyPuffModel } from './squibildy-puff.model';
import { SquibilidyPuffService } from './squibilidy-puff.service';

@Component({
    selector: 'app-squibilidy-puff',
    templateUrl: './squibilidy-puff.component.html',
    styles: [`
        .squib {
            border: 1px black solid;
        }
        .squib-form {
            border-bottom: 1px red dashed;
        }
    `],
    providers: [SquibilidyPuffService]
})
export class SquibilidyPuffComponent implements OnInit {

    myVeryOwnSquib: Observable<SquibildyPuffModel>; //to type
    aBrandNewSquib: Observable<SquibildyPuffModel>; //to type
    myOwnOldSchoolSquib: SquibildyPuffModel;

    // Forms for input testing
    // Future optimisation - https://stackoverflow.com/a/39693615/5760182
    fetchSquib: GimmeAPrimeSquibPuffQueryVariables = {
        squibName: 'Your squibs name',
        baseText: 'yerpyplerps',
        sizeOfText: 10,
    }

    makeSquib: MakeMeASquibilidyPuffMutationVariables = {
        squibName: 'idontneedanewnamefoo',
        randomtextbase: 'burgersarebad',
    }

    oldSchoolSquib: GimmeAPrimeSquibPuffQueryVariables = {
        squibName: 'omegasquibname',
        baseText: 'jambonboeufoeuf',
        sizeOfText: 99,
    }

    constructor(private squibService: SquibilidyPuffService) { }

    ngOnInit(): void {

    }

    getMeASquib(squibNameInput, squibBaseTextInput, squibSizeOfTextInput) {
        const fetchSquibVars: GimmeAPrimeSquibPuffQueryVariables = {
            baseText: squibNameInput.value,
            sizeOfText: squibSizeOfTextInput.value,
            squibName: squibBaseTextInput.value
        };
        this.myVeryOwnSquib = this.squibService.fetchASquib(fetchSquibVars);
    }

    makeMeASquib() {
        this.aBrandNewSquib = this.squibService.makeASquib(this.makeSquib);
    }


    getMeAnOldschoolSquib() {
        this.squibService.fetchASquibOldSchool(
            this.oldSchoolSquib.baseText,
            this.oldSchoolSquib.squibName,
            this.oldSchoolSquib.sizeOfText
        ).subscribe(squibModel => this.myOwnOldSchoolSquib = squibModel);
        // this.myOwnOldSchoolSquib.subscribe(squibModel => {
        //     squibModel.operateOnFullNameForView()
        //     typeo
        // })
    }

}
