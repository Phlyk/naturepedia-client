import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GimmeAPrimeSquibPuffQueryVariables, MakeMeASquibilidyPuffMutationVariables } from 'src/generated/graphql';
import { SquibildyPuffModel } from './squibildy-puff.model';
import { SquibilidyPuffDataService } from './squibilidy-puff.data.service';


/* 
Have to make the difference between a display related retrieving service (with ibuilt cache)
and an operations service
 */
@Injectable()
export class SquibilidyPuffService {
    // fetchedSquib: SquibildyPuffModel;
    // madeSquib: SquibildyPuffModel;
    oldSchoolFetchedSquib: SquibildyPuffModel;

    constructor(private dataService: SquibilidyPuffDataService) {

    }

    fetchASquib(varsToFetchSquib: GimmeAPrimeSquibPuffQueryVariables): Observable<SquibildyPuffModel> {
        return this.dataService.getMeASquibilidyPuff(varsToFetchSquib)
            .pipe(map(squibDto => new SquibildyPuffModel(squibDto)));
            // .subscribe(squibDto => this.fetchedSquib = new SquibildyPuffModel(squibDto));
    }

    makeASquib(makeSquibVars: MakeMeASquibilidyPuffMutationVariables): Observable<SquibildyPuffModel> {
        return this.dataService.makeMeASquibilidyPuff(makeSquibVars)
            .pipe(map(newSquibDto => new SquibildyPuffModel(newSquibDto)))
    }

    fetchASquibOldSchool(baseText: string, squibName?: string, sizeOfText?: number): Observable<SquibildyPuffModel> {
        return new Observable(observer => {
            this.dataService.getMeAnOldSchoolSquibMcPuffingtons(baseText, squibName, sizeOfText)
                .subscribe(squibDto => {
                    this.oldSchoolFetchedSquib = new SquibildyPuffModel(squibDto);
                    observer.next(this.oldSchoolFetchedSquib);
                });
            });
    }
}
