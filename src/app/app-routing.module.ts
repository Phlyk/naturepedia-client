import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FishComponent } from './fish/fish.component';
import { LandingComponent } from './landing/landing.component';
import { SquibilidyPuffComponent } from './squibilidy-puff/squibilidy-puff.component';


const routes: Routes = [
    { path: '', component: LandingComponent },
    { path: 'squib', component: SquibilidyPuffComponent },
    {
        path: 'fish',
        component: FishComponent,
        loadChildren: () => import('./fish/fish.module').then(fm => fm.FishModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
