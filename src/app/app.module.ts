import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ApiModule } from './api/api.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FishModule } from './fish/fish.module';
import { LandingComponent } from './landing/landing.component';
import { SquibilidyPuffInputComponent } from './squibilidy-puff/squibilidy-puff-input/squibilidy-puff-input.component';
import { SquibilidyPuffComponent } from './squibilidy-puff/squibilidy-puff.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,

    SquibilidyPuffComponent,
    SquibilidyPuffInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApiModule,
    HttpClientModule,
    FormsModule,
    // ReactiveFormsModule,
    FishModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
