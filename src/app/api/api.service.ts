import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApiModule } from './api.module';

@Injectable({
    providedIn: ApiModule
})
export class ApiService {
    /* 
        This beautiful puppy will query the Graphene Django (non Relay for now) GraphQL API
        providing all the data for the app
    */
    constructor(
        private apollo: Apollo
    ) { }

    // I want to Apollo in something so I can see raw payloads & DTO mapping...

    // getMeAllDemFishes() {
    //     return this.apollo
    //         .watchQuery<any>({ query: getMeAllDemFishesQuery })
    //         .valueChanges.pipe(
    //             map(({data}) => data.allFishies)
    //         );
    // }

}
